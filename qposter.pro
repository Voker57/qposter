# -------------------------------------------------
# Project created by QtCreator 2008-11-24T17:47:00
# -------------------------------------------------
QT += network \
    script \
    xml \
    xmlpatterns \
    webkit
TARGET = qposter
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    fieldform.cpp \
    poster.cpp \
    newformwizard.cpp \
    webbrowser.cpp \
    formchooserform.cpp \
    htmlform.cpp \
    postingstatuswidget.cpp
HEADERS += mainwindow.h \
    fieldform.h \
    poster.h \
    newformwizard.h \
    webbrowser.h \
    formchooserform.h \
    htmlform.h \
    postingstatuswidget.h
FORMS += mainwindow.ui \
    formfield.ui \
    webbrowser.ui \
    formchooserform.ui
RESOURCES += qposter.qrc
