/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef FORMCHOOSERFORM_H
#define FORMCHOOSERFORM_H

#include "ui_formchooserform.h"
#include "htmlform.h"

#include <QtGui/QWizardPage>

class QPlainTextEdit;
class QLineEdit;

class FormChooserForm : public QWizardPage, public Ui::FormChooserForm
{
	Q_OBJECT
public:
	FormChooserForm(QWidget * parent = 0);
	bool isComplete() const;
public slots:
	void reparseHtml();
	void emitSelectedForm();
signals:
	void formReady(HtmlForm);
protected:
	QList<HtmlForm> formList;
};

#endif // FORMCHOOSERFORM_H
