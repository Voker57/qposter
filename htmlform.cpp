/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#include "htmlform.h"

#include <QtCore/QRegExp>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QUrl>

HtmlForm::HtmlForm()
{

}

HtmlForm::~HtmlForm()
{

}

QPair<QString, QString> HtmlForm::pairFromList(QStringList list)
{
	if(!list.at(1).isEmpty())
		return QPair<QString, QString>(list.at(1), list.at(2));
	if(!list.at(3).isEmpty())
		return QPair<QString, QString>(list.at(3), list.at(4));
	if(!list.at(5).isEmpty())
		return QPair<QString, QString>(list.at(5), list.at(6));
	else return QPair<QString, QString>();
}

QList<HtmlForm> HtmlForm::listFromHtml(QString html, QString url)
{
	QRegExp formRgx("<form ([^>]*)>(.*)</form>");
	QRegExp inputRgx("<input ([^>]*)>|<textarea([^>]*)>([^<]*)</textarea>");
	inputRgx.setMinimal(true);
	inputRgx.setCaseSensitivity(Qt::CaseInsensitive);
	QRegExp parameterRgx("([a-z]+)=(\\w+)|([a-z]+)=\"(.*)\"|([a-z]+)='(.*)'");
	parameterRgx.setMinimal(true);
	parameterRgx.setCaseSensitivity(Qt::CaseInsensitive);
	formRgx.setMinimal(true);
	formRgx.setCaseSensitivity(Qt::CaseInsensitive);
	int pos = 0;
	QList<HtmlForm> formList;
	while ((pos = formRgx.indexIn(html, pos)) != -1)
	{
		// Capture each form
		HtmlForm form;
		int pos0 = 0;
		while((pos0 = parameterRgx.indexIn(formRgx.cap(1), pos0)) != -1)
		{
			QPair<QString, QString> pair = pairFromList(parameterRgx.capturedTexts());
			if(!pair.first.compare("method", Qt::CaseInsensitive)) form.method = pair.second;
			if(!pair.first.compare("action", Qt::CaseInsensitive))
			{
				QUrl pageUrl(url);
				QUrl tgtUrl(pair.second);
				if(tgtUrl.isRelative())
				{
					if(!pair.second.startsWith('/'))
						tgtUrl.setPath(pageUrl.path().replace(QRegExp("[^/]+$"), "") + tgtUrl.path()); // For href="mycrappyfile.php"
					tgtUrl.setScheme(pageUrl.scheme());
					tgtUrl.setHost(pageUrl.host());
				}
				form.targetUrl=tgtUrl.toString();
			}
			pos0 +=parameterRgx.matchedLength();
		}
		pos0 = 0;
		while((pos0 = inputRgx.indexIn(formRgx.cap(2), pos0)) != -1)
		{
			int pos1 = 0;
			QString f00d = inputRgx.cap(1).isEmpty() ? inputRgx.cap(2) : inputRgx.cap(1);
			QHash<QString, QString> inpHash;
			while((pos1 = parameterRgx.indexIn(f00d, pos1)) != -1)
			{
				QPair<QString, QString> pair = pairFromList(parameterRgx.capturedTexts());
				inpHash[pair.first.toLower()] = pair.second;
				pos1 += parameterRgx.matchedLength();
			}
			form.fields[inpHash["name"]] = formRgx.cap(1).isEmpty() ? inputRgx.cap(3) : inpHash["value"];
			pos0 +=inputRgx.matchedLength();
		}
	/*	qDebug("FORM BEGIN");
		qDebug() << form.method;
		qDebug() << form.targetUrl;
		qDebug() << form.fields;
		qDebug("FORM END"); */
		formList << form;
		pos += formRgx.matchedLength();
	}
	return formList;
}
