/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef HTMLFORM_H
#define HTMLFORM_H

#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QPair>

class HtmlForm
{
public:
	HtmlForm();
	~HtmlForm();
	static QList<HtmlForm> listFromHtml(QString html, QString url);
	static QPair<QString, QString> pairFromList(QStringList list);
public:
	QString method;
	QString targetUrl;
	QHash<QString, QString> fields;
};

#endif // HTMLFORM_H
