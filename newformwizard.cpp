/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/

#include "newformwizard.h"
#include "webbrowser.h"
#include "formchooserform.h"

#include <QtGui/QPixmap>
#include <QtGui/QVBoxLayout>

NewFormWizard::NewFormWizard(QWidget * parent) : QWizard(parent)
{
	setPixmap(QWizard::WatermarkPixmap, QPixmap(":/qposter/wizard.png"));
	setWindowTitle(tr("New form wizard"));
	webBrowsingPage = createWebBrowsingPage();
	connect(button(QWizard::NextButton), SIGNAL(clicked()), this, SLOT(setHtmlIfBrowsing()));
	connect(button(QWizard::FinishButton), SIGNAL(clicked()), this, SLOT(aboutToFinish()));
	addPage(createFirstPage());
	addPage(webBrowsingPage);
	addPage(createFormChooserPage());
	connect(formChooserPage, SIGNAL(formReady(HtmlForm)), this, SIGNAL(formReady(HtmlForm)));
	show();
}

QWizardPage * NewFormWizard::createFirstPage()
{
	QWizardPage * page = new QWizardPage();
	page->setTitle("Welcome to new form wizard!");
	page->setSubTitle("It will make you happy and take your troubles away blah blah blah");
	return page;
}

QWizardPage * NewFormWizard::createWebBrowsingPage()
{
	QWizardPage * page = new QWizardPage();
	page->setTitle("Please browse to target page here.");
	page->setSubTitle("When you are in place, press 'Ready'");
	QVBoxLayout * layout = new QVBoxLayout();
	webBrowserWidget = new WebBrowser();
	layout->addWidget(webBrowserWidget);
	page->setLayout(layout);
	return page;
}

QWizardPage * NewFormWizard::createFormChooserPage()
{
	formChooserPage = new FormChooserForm;
	return formChooserPage;
}

void NewFormWizard::setHtmlIfBrowsing()
{
	if(currentId()==2)
	{
		setField("HTML", QVariant(webBrowserWidget->getHtml()));
		setField("URI", QVariant(webBrowserWidget->getUrl()));
		formChooserPage->reparseHtml();
	}
}

void NewFormWizard::aboutToFinish()
{
	formChooserPage->emitSelectedForm();
}
