/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/

#include "fieldform.h"

#include <QtCore/QRegExp>

FieldForm::FieldForm(QWidget * parent) : QWidget(parent)
{
	setupUi(this);
	valueGroup->hide();
}

void FieldForm::removeClicked()
{
	emit removed();
}

QString FieldForm::fieldName()
{
	return fieldEdit->text();
}

QString FieldForm::fullValue()
{
	return valueEdit->toPlainText();
}

QString FieldForm::fieldValue()
{
	if(regexpCheckBox->isChecked())
	{
		QStringList values = fullValue().split(QRegExp(splitterEdit->text()));
		return values[qrand()%values.size()];
	} else return fullValue();
}

void FieldForm::toggleValueGroup()
{
	if(valueGroup->isHidden())
		valueGroup->show();
	else
		valueGroup->hide();
}

void FieldForm::setFieldName(QString value)
{
	fieldEdit->setText(value);
}

void FieldForm::setFullValue(QString value)
{
	valueEdit->setPlainText(value);
}

bool FieldForm::isRandom()
{
	return regexpCheckBox->isChecked();
}

void FieldForm::setRandom(bool value)
{
	regexpCheckBox->setChecked(value);
}
