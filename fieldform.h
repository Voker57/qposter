/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef FIELDFORM_H
#define FIELDFORM_H

#include "ui_formfield.h"

class FieldForm : public QWidget, public Ui::FieldForm
{
	Q_OBJECT
public:
	FieldForm(QWidget * parent = 0);
	QString fieldName();
	QString fullValue();
	QString fieldValue();
	void setFieldName(QString);
	void setFullValue(QString);
	bool isRandom();
	void setRandom(bool value);
signals:
	void removed();
protected slots:
	void removeClicked();
	void toggleValueGroup();
};

#endif // FIELDFORM_H
