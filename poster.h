/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef POSTER_H
#define POSTER_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QString>

class QNetworkAccessManager;
class QNetworkReply;

class Poster : public QObject
{
	Q_OBJECT
public:
	Poster(QObject * parent);
	QNetworkReply * post(QString address, QHash<QString, QString> data);
protected:
	QNetworkAccessManager * netManager;
};

#endif // POSTER_H
