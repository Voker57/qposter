/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#include "postingstatuswidget.h"
#include "ui_postingstatuswidget.h"

#include <QtNetwork/QNetworkReply>
#include <QtWebKit/QWebView>
#include <QtGui/QMouseEvent>

PostingStatusWidget::PostingStatusWidget(QWidget *parent, QNetworkReply * reply) : QProgressBar(parent)
{
	netReply = reply;
	if(netReply)
	connect(netReply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
}

void PostingStatusWidget::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	if(bytesTotal)
		setValue(100 * (bytesReceived/bytesTotal));
}

void PostingStatusWidget::mouseReleaseEvent(QMouseEvent * event)
{
	if(event->button() == Qt::RightButton)
		deleteLater();
	if(event->button() == Qt::LeftButton)
	{
		QWebView * responseView = new QWebView();
		responseView->setAttribute(Qt::WA_DeleteOnClose, true);
		responseView->setHtml(QString(netReply->readAll()));
		responseView->show();
	}
	QWidget::mouseReleaseEvent(event);
}
