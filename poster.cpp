/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#include "poster.h"

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtCore/QUrl>
#include <QtCore/QByteArray>
#include <QtCore/QDebug>

Poster::Poster(QObject * parent) : QObject(parent)
{
	netManager = new QNetworkAccessManager(this);
}

QNetworkReply * Poster::post(QString address, QHash<QString, QString> data)
{
	QByteArray dat;
	QHash<QString, QString>::const_iterator i;
	for(i=data.constBegin(); i !=data.constEnd(); ++i)
		if(i.key().size() && i.value().size())
			dat.append(QString("%1=%2&").arg(QString(QUrl::toPercentEncoding(i.key())), QString(QUrl::toPercentEncoding(i.value()))));
	dat.remove(dat.size()-1,1);
	return netManager->post(QNetworkRequest(QUrl(address)), dat);
}
