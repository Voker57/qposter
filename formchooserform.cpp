/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#include "formchooserform.h"
#include "ui_formchooserform.h"

FormChooserForm::FormChooserForm(QWidget *parent) :
QWizardPage(parent)
{
	setupUi(this);
	// TODO: htmledit should be plaintextedit, but i was tired of fucking with Q_PROPERTY
	registerField("HTML", htmlEdit);
	registerField("URI", urlEdit);
	connect(formsList, SIGNAL(currentRowChanged(int)), this, SIGNAL(completeChanged()));
}

void FormChooserForm::reparseHtml()
{
	formsList->clear();
	formList=HtmlForm::listFromHtml(htmlEdit->text(), urlEdit->text());
	for(QList<HtmlForm>::const_iterator it = formList.begin(); it!=formList.end(); ++it)		{
		formsList->addItem( QString("=> %1, fields: %3").arg(
				(*it).targetUrl,
				QStringList((*it).fields.keys()).join(",")
		) );
	}
}

bool FormChooserForm::isComplete() const
{
	return (formsList->currentRow() != -1);
}

void FormChooserForm::emitSelectedForm()
{
	emit formReady(formList.at(formsList->currentRow()));
}
