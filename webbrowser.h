/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef WEBBROWSER_H
#define WEBBROWSER_H

#include "ui_webbrowser.h"

#include <QtGui/QWizardPage>

class WebBrowser : public QWidget, public Ui::TinyWebBrowser
{
	Q_OBJECT
public:
	WebBrowser(QWidget * parent = 0);
	QString getHtml();
	QString getUrl();
protected slots:
	void goToUrl();
};

#endif // WEBBROWSER_H
