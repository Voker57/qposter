/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include "wiper.h"
#include "htmlform.h"

#include <QtGui/QMainWindow>
#include <QtCore/QHash>

class FieldForm;
class QFileDialog;

class MainWindow : public QMainWindow, public Ui::MainWindowClass
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
	~MainWindow();
public slots:
	void addField();
	void removeField();
	void wipePressed();
	void clearFormData();
protected slots:
	void callWizard();
	void gotForm(HtmlForm);
	void openFile();
	void saveFile();
protected:
	QList<FieldForm *> fieldForms;
	Wiper * wiper;

};

#endif // MAINWINDOW_H
