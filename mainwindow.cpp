/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#include "mainwindow.h"
#include "fieldform.h"
#include "newformwizard.h"
#include "postingstatuswidget.h"

#include <QFileDialog>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	setupUi(this);
	wiper = new Wiper(this);
}

MainWindow::~MainWindow()
{

}

void MainWindow::addField()
{
	FieldForm * fieldForm = new FieldForm(this);
	fieldForms << fieldForm;
	connect(fieldForm, SIGNAL(removed()), this, SLOT(removeField()));
	fieldsLayout->addWidget(fieldForm);
	fieldForm->show();
}

void MainWindow::removeField()
{
	fieldForms.removeAll((FieldForm *)sender());
	sender()->deleteLater();
}

void MainWindow::wipePressed()
{
	QHash<QString, QString> hash;
	QList<FieldForm *>::iterator i;
	for(i = fieldForms.begin(); i != fieldForms.end(); ++i)
		hash[(*i)->fieldName()] = (*i)->fieldValue();
	statusLayout->addWidget(new PostingStatusWidget(0, wiper->wipe(urlEdit->text(),hash)));
}

void MainWindow::callWizard()
{
	connect(new NewFormWizard(this), SIGNAL(formReady(HtmlForm)), this, SLOT( gotForm(HtmlForm)));
}

void MainWindow::gotForm(HtmlForm form)
{
	// form -> main window
	clearFormData();
	for(QHash<QString, QString>::const_iterator it = form.fields.begin(); it != form.fields.end(); ++it)
	{
		addField();
		fieldForms.last()->setFieldName(it.key());
		fieldForms.last()->setFullValue(it.value());
	}
	urlEdit->setText(form.targetUrl);
}

void MainWindow::clearFormData()
{
	fieldForms.clear();
}

void MainWindow::openFile()
{
	QSettings fileToOpen(QFileDialog::getOpenFileName(this, tr("Open form"), QString(), tr("Form data (*.ini)")), QSettings::IniFormat, this);
	urlEdit->setText(fileToOpen.value("url").toString());
	clearFormData();
	int size=fileToOpen.beginReadArray("fields");
	for(int i=0; i < size; ++i)
	{
		fileToOpen.setArrayIndex(i);
		addField();
		fieldForms.last()->setFieldName(fileToOpen.value("name").toString());
		fieldForms.last()->setFullValue(fileToOpen.value("value").toString());
		fieldForms.last()->setRandom(fileToOpen.value("random").toBool());
	}
	fileToOpen.endArray();
}

void MainWindow::saveFile()
{
	QSettings fileToSave(QFileDialog::getSaveFileName(this, tr("SaveAs"), QString(), tr("Form data (*.ini)")), QSettings::IniFormat, this);
	fileToSave.setValue("url", QVariant(urlEdit->text()));
	fileToSave.beginWriteArray("fields");
	for(int i = 0; i < fieldForms.size(); i++)
	{
		fileToSave.setArrayIndex(i);
		fileToSave.setValue("name", QVariant(fieldForms[i]->fieldName()));
		fileToSave.setValue("value", QVariant(fieldForms[i]->fullValue()));
		fileToSave.setValue("random", QVariant(fieldForms[i]->isRandom()));
	}
	fileToSave.endArray();
}
