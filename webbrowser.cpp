/*	This file is part of qposter.

	Copyright Voker57 2008

	qposter is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	qposter is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with qposter.  If not, see <http://www.gnu.org/licenses/>
*/
#include "webbrowser.h"

#include <QtWebKit/QWebView>
#include <QtWebKit/QWebFrame>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLineEdit>
#include <QtCore/QUrl>
#include <QtGui/QPushButton>

WebBrowser::WebBrowser(QWidget * parent) : QWidget(parent)
{
	setupUi(this);
	webView->setMaximumHeight(400);
}

void WebBrowser::goToUrl()
{
	webView->setUrl(QUrl(urlEdit->text()));
}

QString WebBrowser::getHtml()
{
	return webView->page()->currentFrame()->toHtml();
}

QString WebBrowser::getUrl()
{
	return webView->url().toString();
}
